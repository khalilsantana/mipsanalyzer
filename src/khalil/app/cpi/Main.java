package khalil.app.cpi;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) throws IOException {
        FileReader fr = new FileReader("ASM/mdc.dump");
        BufferedReader br = new BufferedReader(fr);
        String line;
        ArrayList<MIPSInstruction> instructions = new ArrayList<>();
        while ((line = br.readLine()) != null) {
            instructions.add(MIPSInstruction.create(line));
        }
        double inval = 0;
        double rtype = 0;
        double itype = 0;
        double jtype = 0;
        Map<Operation, Integer> hashMap = new HashMap<>();
        for (MIPSInstruction instruction : instructions) {
            System.out.println(instruction);
            if (!hashMap.containsKey(instruction.operation)) {
                hashMap.put(instruction.operation, 1);
            } else {
                int count = hashMap.get(instruction.operation);
                hashMap.put(instruction.operation, count + 1);
            }
            switch (instruction.operation.type) {
                case INVALID: inval++; break;
                case REGISTER: rtype++; break;
                case IMEDIATE: itype++; break;
                case JUMP: jtype++; break;
            }
        }
        double total = instructions.size();
        System.out.println("Instructions per type:");
        System.out.printf("TOTAL: %.0f Percentage: %.2f%n", total, (total / total) * 100);
        System.out.printf("INVAL: %.0f Percentage: %.2f%n", inval, (inval / total) * 100);
        System.out.printf("RTYPE: %.0f Percentage: %.2f%n", rtype, (rtype / total) * 100);
        System.out.printf("ITYPE: %.0f Percentage: %.2f%n", itype, (itype / total) * 100);
        System.out.printf("JTYPE: %.0f Percentage: %.2f%n", jtype, (jtype / total) * 100);
        System.out.println("Cycles spent per instruction type:");
        Integer cycles = 0;
        for (Map.Entry<Operation, Integer> entry : hashMap.entrySet()) {
            int lCost = entry.getKey().cost * entry.getValue();
            System.out.printf("%s Count: %d Cost: %d Cycles: %d\n", entry.getKey(), entry.getValue(), entry.getKey().cost, lCost);
            cycles += lCost;
        }
        System.out.printf("Overall Cycles: %d\n", cycles);
        System.out.printf("CPI: %f\n", cycles/total);
    }
}
