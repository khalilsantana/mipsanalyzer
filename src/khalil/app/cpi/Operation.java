package khalil.app.cpi;

public enum Operation {
    INVALID("", InstructionType.INVALID, FunctionType.INVALID, 0),
    SLT("000000", InstructionType.REGISTER, FunctionType.SLT, 4),
    SUB("000000", InstructionType.REGISTER, FunctionType.SUB, 4),
    ADDIU("001001", InstructionType.IMEDIATE, FunctionType.INVALID, 4),
    JUMP("000010", InstructionType.JUMP, FunctionType.INVALID, 3),
    BEQ("000100", InstructionType.IMEDIATE, FunctionType.INVALID, 3);

    public String opcode;
    public InstructionType type;
    public FunctionType funct;
    public Integer cost;

    Operation(String opcode, InstructionType type, FunctionType functionType, int cost) {
        this.opcode = opcode;
        this.type = type;
        this.funct = functionType;
        this.cost = cost;
    }

    @Override
    public String toString() {
        return "Operation{'" + name() + "'}'";
    }
}
