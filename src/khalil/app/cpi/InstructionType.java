package khalil.app.cpi;

public enum InstructionType {
    INVALID,
    REGISTER,
    IMEDIATE,
    JUMP;
}
