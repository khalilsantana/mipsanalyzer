package khalil.app.cpi;

public enum FunctionType {
    INVALID(""),
    SLL("000000"),
    SLT("101010"),
    SUB("100010"),
    JR("001000");
    String funct;

    FunctionType(String funct) {
        this.funct = funct;
    }
}
