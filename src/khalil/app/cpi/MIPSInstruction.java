package khalil.app.cpi;

public class MIPSInstruction {
    public final String instruction;
    public Operation operation;

    MIPSInstruction(String instruction) {
        this.instruction = instruction;
    }

    @Override
    public String toString() {
        return "MIPSInstruction{" +
                "instruction='" + instruction + '\'' +
                ", operation=" + operation.name() +
                ", type=" + operation.type +
                ", funct=" + operation.funct +
                '}';
    }

    public static MIPSInstruction create(String instruction) {
        MIPSInstruction ins = new MIPSInstruction(instruction);
        String opcode = instruction.substring(0, 6);
        if (opcode.equals("000000")) {
            ins.operation = lookupByFunct(instruction.substring(26, 32));
        } else {
            ins.operation = lookupByOpcode(opcode);
        }
        return ins;
    }

    private static Operation lookupByFunct(String funct) {
        for (Operation op : Operation.values()) {
            if (funct.equals(op.funct.funct)) {
                return op;
            }
        }
        return Operation.INVALID;
    }

    private static Operation lookupByOpcode(String opcode) {
        for (Operation op : Operation.values()) {
            if (opcode.equals(op.opcode)) {
                return op;
            }
        }
        return Operation.INVALID;
    }
}

