# Disciplina: Arquitetura e Organização de Computadores
# Atividade: Avaliação 02 - M1-P2 - Desempenho
# Autor: Khalil G. Q. de Santana
# Arquivo: mdc.asm
.text
main: 
    li      $s0, 16          # X <- 16
    li      $s1, 24          # Y <- 24
while:    
    beq     $s0, $s1, end
    bge	    $s1, $s0, suby   # if (Y >= $s0) then suby
subx:
    sub     $s0, $s0, $s1    # X <- X - Y
    j       while
suby:
    sub     $s1, $s1, $s0    # Y <- Y - X
    j       while
end: 
    
