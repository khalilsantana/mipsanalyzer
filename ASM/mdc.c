int main() {
    int x = 16;
    int y = 24;
    while (x != y) {
        if (x < y) {
            y = y - x;
        }
        else {
            x = x - y;
        }
    }
    return x;
}